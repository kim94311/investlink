from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from orders.consumers import OrderStatusConsumer


application = ProtocolTypeRouter(
    {
        "websocket": URLRouter(
            [
                path("ws/orders/", OrderStatusConsumer.as_asgi()),
            ]
        ),
    }
)
