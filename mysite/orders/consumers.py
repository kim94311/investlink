import json
import os
import requests
from channels.generic.websocket import AsyncWebsocketConsumer
from orders.models import Order
import base64


api_key = os.environ.get('ALPACA_API_KEY')
api_secret = os.environ.get('ALPACA_API_SECRET')


if not api_key or not api_secret:
    raise ValueError("ALPACA_API_KEY and ALPACA_API_SECRET must be set")

credentials = f"{api_key}:{api_secret}"

encoded_credentials = base64.b64encode(
    credentials.encode("utf-8")).decode("utf-8")


class OrderStatusConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()
        url = "https://broker-api.sandbox.alpaca.markets/v1/events/trades"

        headers = {
            "accept": "text/event-stream",
            "authorization": f"Basic {encoded_credentials}"
        }

        async with requests.get(url, headers=headers, stream=True) as response:
            if response.status_code == 200:
                async for chunk in response.iter_content(chunk_size=None, decode_unicode=True):
                    if chunk:
                        try:
                            order_data = json.loads(chunk)
                            for order in order_data['orders']:
                                Order.objects.update_or_create(
                                    id=order['id'],
                                    defaults=order
                                )
                            await self.send(text_data=json.dumps({"orders": order_data}))
                        except json.JSONDecodeError:
                            pass

    async def disconnect(self, close_code):
        pass
