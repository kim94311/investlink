from django.db import models


# Create your models here.
class Order(models.Model):
    id = models.CharField(max_length=255, blank=False,
                          null=False, primary_key=True)
    client_order_id = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    submitted_at = models.DateTimeField()
    filled_at = models.DateTimeField(blank=True, null=True)
    expired_at = models.DateTimeField(blank=True, null=True)
    canceled_at = models.DateTimeField(blank=True, null=True)
    failed_at = models.DateTimeField(blank=True, null=True)
    replaced_at = models.DateTimeField(blank=True, null=True)
    replaced_by = models.CharField(max_length=255, blank=True, null=True)
    replaces = models.CharField(max_length=255, blank=True, null=True)
    asset_id = models.CharField(max_length=255)
    symbol = models.CharField(max_length=255)
    asset_class = models.CharField(max_length=255)
    notional = models.CharField(max_length=255, blank=True, null=True)
    qty = models.CharField(max_length=255, blank=True, null=True)
    filled_qty = models.CharField(max_length=255)
    filled_avg_price = models.CharField(max_length=255, blank=True, null=True)
    order_class = models.CharField(max_length=255, blank=True, null=True)
    order_type = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    side = models.CharField(max_length=255)
    time_in_force = models.CharField(max_length=255)
    limit_price = models.CharField(max_length=255, blank=True, null=True)
    stop_price = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=255)
    extended_hours = models.BooleanField()
    legs = models.JSONField(blank=True, null=True)
    trail_percent = models.JSONField(blank=True, null=True)
    trail_price = models.JSONField(blank=True, null=True)
    hwm = models.JSONField(blank=True, null=True)
    commission = models.CharField(max_length=255, blank=True, null=True)
    commission_bps = models.CharField(max_length=255, blank=True, null=True)
    subtag = models.CharField(max_length=255, blank=True, null=True)
    source = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f"Order {self.id} - {self.status}"
