from django.urls import path
from orders.views import ListOrderAPI, CreateOrderAPI, CancelOrderAPI


urlpatterns = [
    path('orders/', ListOrderAPI.as_view(), name='orders-list'),
    path('orders/create/', CreateOrderAPI.as_view(), name='orders-create'),
    path('orders/cancel/', CancelOrderAPI.as_view(), name='orders-cancel'),
]
