import requests
from rest_framework import generics
from orders.serializer import OrderSerializer
from rest_framework.views import APIView
from django.http import JsonResponse
from orders.models import Order
from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiTypes
import os
import base64


ALPACA_BROKER_ID = '5e62581b-1a60-4f8d-a950-4efa12d15be7'
ALPACA_API_BASE_URL = 'https://broker-api.sandbox.alpaca.markets/v1/trading/accounts'


api_key = os.environ.get('ALPACA_API_KEY')
api_secret = os.environ.get('ALPACA_API_SECRET')
if not api_key or not api_secret:
    raise ValueError("ALPACA_API_KEY and ALPACA_API_SECRET must be set")
credentials = f"{api_key}:{api_secret}"
encoded_credentials = base64.b64encode(
    credentials.encode("utf-8")).decode("utf-8")
# Create your views here.


class ListOrderAPI(generics.ListAPIView):
    serializer_class = OrderSerializer

    def get_queryset(self):
        # Выполняем GET-запрос к API Alpaca
        url = f'{ALPACA_API_BASE_URL}/{ALPACA_BROKER_ID}/orders?status=all'

        headers = {
            "accept": "application/json",
            "authorization": f"Basic {encoded_credentials}"
        }

        try:
            response = requests.get(url, headers=headers)
            response.raise_for_status()

            data = response.json()
            if data:
                for order_data in data:
                    # Пытаемся обновить или создать объект Order
                    Order.objects.update_or_create(
                        id=order_data['id'],
                        defaults=order_data
                    )
                return response.json()
            else:
                print("Received empty data from API.")
                return []
        except requests.RequestException as e:
            print(f"Failed to retrieve orders. Error: {e}")
            return []


class CreateOrderAPI(APIView):
    def post(self, request):
        account_id = request.data.get('account_id')
        symbol = request.data.get('symbol')
        qty = request.data.get('qty')
        side = request.data.get('side')
        type = request.data.get('type')
        time_in_force = request.data.get('time_in_force')
        payload = {
            "side": f"{side}",
            "type": f"{type}",
            "time_in_force": f"{time_in_force}",
            "symbol": f"{symbol.upper()}",
        }
        if qty:
            payload['qty'] = f"{qty}"
        if not qty:
            notional = request.data.get('notional')
            payload["notional"] = f"{notional}"
        if type == 'limit':
            try:
                limit_price = request.data.get('limit_price')
                payload['limit_price'] = f"{limit_price}"
            except Exception:
                return JsonResponse({'message': 'Limit price is required for limit orders.'})
        if type == 'stop':
            try:
                stop_price = request.data.get('stop_price')
                payload['stop_price'] = f"{stop_price}"
            except Exception:
                return JsonResponse({'message': 'Stop price is required for stop orders.'})
        if type == 'stop_limit':
            try:
                stop_price = request.data.get('stop_price')
                limit_price = request.data.get('limit_price')
                payload['limit_price'] = f"{limit_price}"
                payload['stop_price'] = f"{stop_price}"
            except Exception:
                return JsonResponse({'message': 'Stop price and limit price are required for stop_limit orders.'})
        if type == 'trailing_stop':
            try:
                trail_percent = request.data.get('trail_percent')
                payload['trail_percent'] = f"{trail_percent}"
                if not trail_percent:
                    trail_price = request.data.get('trail_price')
                    payload['trail_price'] = f"{trail_price}"
            except Exception:
                return JsonResponse({'message': 'Trailing percent is required for trailing_stop orders.'})
        subtag = request.data.get('subtag')
        if subtag:
            payload["subtag"] = f"{subtag}"

        url = f'{ALPACA_API_BASE_URL}/{account_id}/orders'

        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "authorization": f"Basic {encoded_credentials}"
        }

        print(payload)
        response = requests.post(url, json=payload, headers=headers)
        if response.status_code == 201:
            return JsonResponse({'message': 'Order created successfully.'})
        else:
            return JsonResponse({'message': f'{response.text}.'})


class CancelOrderAPI(APIView):
    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="order_id", description="ID of the order to be canceled",
                required=True, type=OpenApiTypes.STR
            ),
            OpenApiParameter(
                name="account_id", description="ID of the account",
                required=True, type=OpenApiTypes.STR
            ),
        ],
        responses={200: {"description": "Order canceled successfully."}},
    )
    def post(self, request):
        account_id = request.data.get('account_id')
        order_id = request.data.get('order_id')

        url = f'{ALPACA_API_BASE_URL}/{account_id}/orders/{order_id}'

        headers = {
            "accept": "application/json",
            "authorization": f"Basic {encoded_credentials}"
        }

        response = requests.delete(url, headers=headers)
        if response.status_code == 204:
            order = Order.objects.filter(id=order_id).first()
            if order:
                order.status = 'canceled'
                order.save()
            return JsonResponse({'message': 'Order canceled successfully.'})
        elif response.status_code == 400:
            return JsonResponse({'message': 'Order or account does not exist.'}, status=400)
        else:
            return JsonResponse({'message': 'Failed to cancel order.'}, status=500)
