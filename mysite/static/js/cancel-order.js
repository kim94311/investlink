$(document).ready(function () {
    $('.button-cancel-order').click(cancelOrder);
    function cancelOrder () {
        var account_id = $('#cancel_account_id').val();
        var order_id = $('#cancel_order_id').val();
        data = {
            'account_id': account_id,
            'order_id': order_id
        }
        $.ajax({
            method: 'POST',
            url: 'http://127.0.0.1:8000/api/orders/cancel/',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data) {
                console.log("Received data:", data);
            },
            error: function (error) {
                console.error("Error fetching data:", error);
            }
        })
    }
})