$(document).ready(function () {
    $('.button-create-order').click(createOrder);

    function createOrder() {
        var account_id = $('#account_id').val();
        var symbol = $('#symbol').val();
        var qty = $('#qty').val();
        var notional = $('#notional').val();
        var time_in_force = $('#time_in_force').val();
        var order_type = $('#order_type').val();
        var subtag = $('#subtag').val();
        var side = $('#side').val();
        console.log(notional);
        data = {
            'side': side,
            'account_id': account_id,
            'symbol': symbol,
            'qty': qty,
            'notional': notional,
            'time_in_force': time_in_force,
            'type': order_type,
            'subtag': subtag
        }

        // Проверка наличия qty и notional
        if ((qty !== "" && notional === "") || (qty === "" && notional !== "")) {
            alert("Please enter both qty and notional");
            return;
        }
        $.ajax({
            method: 'POST',
            url: 'http://127.0.0.1:8000/api/orders/create/',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (data) {
                console.log("Received data:", data);
            },
            error: function (error) {
                console.error("Error fetching data:", error);
            }
        });
    }
});