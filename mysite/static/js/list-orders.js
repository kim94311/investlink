$(document).ready(function () {
    function listOrders() {
        $.ajax({
            method: 'GET',
            url: 'http://127.0.0.1:8000/api/orders/',
            contentType: 'application/json',
            success: function (data) {
                console.log("Received data:", data);
                
                data.forEach(order => {
                    console.log(order)
                })
            },
            error: function (error) {
                console.error("Error fetching data:", error);
            }
        })
    }
    listOrders()
})